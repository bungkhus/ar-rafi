package cukil.ar_rafi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class LoginActivity extends AppCompatActivity {
    EditText inUsername, inPasword;
    Button bLogin;
    String response = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        inUsername = (EditText) findViewById(R.id.inUsername);
        inPasword = (EditText) findViewById(R.id.inPassword);
        bLogin = (Button) findViewById(R.id.bLogin);
        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ConnectionHelper().execute();
            }
        });
    }

    private String getQuery(List<Pair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Pair<String, String> pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.first, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.second, "UTF-8"));
        }

        return result.toString();
    }

    private class ConnectionHelper extends AsyncTask<String, Void, String> {
        String username, password;
        ProgressDialog p = new ProgressDialog(LoginActivity.this);
        @Override
        protected void onPreExecute() {
            response = "";
            username = inUsername.getText().toString();
            password = inPasword.getText().toString();
            p.setTitle("Tunggu sebentar...");
            p.show();
        }

        @Override
        protected String doInBackground(String... params) {
            URL url = null;
            try {
                url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=login");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                List<Pair> paramss = new ArrayList<Pair>();
                paramss.add(new Pair<String, String>("username", username));
                paramss.add(new Pair<String, String>("password", password));

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getQuery(paramss));
                writer.flush();
                writer.close();
                os.close();

                conn.connect();
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line=br.readLine()) != null) {
                        response+=line;
                    }
                } else {
                    response = "";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            p.cancel();
            if (result != "") {
                try {
                    JSONObject json = new JSONObject(result);
                    String login_success = json.getString("login_success");
                    if (login_success.equals("1")) {
                        String username = json.getString("login_username");
                        String password = json.getString("login_password");
                        String status = json.getString("login_status");
                        Intent i = null;
                        switch (status) {
                            case "KaTu":
                                i = new Intent(LoginActivity.this, KatuActivity.class);
                                break;
                            case "caraka":
                                i = new Intent(LoginActivity.this, CarakaActivity.class);
                                break;
                            case "satpam":
                                i = new Intent(LoginActivity.this, SatpamActivity.class);
                                break;
                            default:
                                break;
                        }
                        Bundle b = new Bundle();
                        b.putString("username", inUsername.getText().toString());
                        b.putString("status", status);
                        i.putExtras(b);
                        startActivity(i);
                        finish();
//                        Toast.makeText(getApplicationContext(), "Berhasil", Toast.LENGTH_SHORT).show();
                    }
                    else
                        Toast.makeText(getApplicationContext(), "Username / password salah", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
