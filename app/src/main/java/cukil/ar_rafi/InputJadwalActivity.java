package cukil.ar_rafi;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class InputJadwalActivity extends AppCompatActivity {

    EditText inTglMulaiJadwal, inTglSelesaiJadwal;
    Spinner listUsernameJadwal, listTugasJadwal, listTugasJadwalCaraka,spinner;
    Button bInputJadwal;
    String pilih = "SATPAM";
    String response, aksi, tglMasukFinal, tglSelesaiFinal, usernameFinal, username, status, tugasFinal;
    SimpleDateFormat dateFormat, dateFormat2;
    ArrayList<String> listUsername = new ArrayList<String>();
    ArrayList<String> listNama = new ArrayList<String>();
    ArrayAdapter<String> listUsernameAdapter;
    final Calendar c_mulai = Calendar.getInstance();
    final Calendar c_selesai = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_jadwal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        inTglMulaiJadwal = (EditText) findViewById(R.id.inTglMulaiJadwal);
        inTglSelesaiJadwal = (EditText) findViewById(R.id.inTglSelesaiJadwal);
        listUsernameJadwal = (Spinner) findViewById(R.id.listUsernameJadwal);
        listTugasJadwal = (Spinner) findViewById(R.id.listTugasJadwalSatpam);
        listTugasJadwalCaraka = (Spinner) findViewById(R.id.listTugasJadwalCaraka);
        bInputJadwal = (Button) findViewById(R.id.bInputJadwal);
        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.jadwal_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        tugasFinal = listTugasJadwal.getSelectedItem().toString();
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                pilih = adapterView.getSelectedItem().toString();
                onPilihJadwal(pilih);
                new ConnectionHelper().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        listTugasJadwal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                onPenugasanClick(adapterView.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        listTugasJadwalCaraka.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                onPenugasanClick(adapterView.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        inTglMulaiJadwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog d = new DatePickerDialog(InputJadwalActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                        tugasFinal = listTugasJadwal.getSelectedItem().toString();
                        System.out.println("TUGAS = "+tugasFinal);
                        Calendar c2 = Calendar.getInstance();
                        c2.set(year, monthOfYear, dayOfMonth);
                        if(tugasFinal.equalsIgnoreCase("Satpam Pagi")){
                            c2.set(year, monthOfYear, dayOfMonth, 6, 0, 0);
                        }else if(tugasFinal.equalsIgnoreCase("Satpam Malam")){
                            c2.set(year, monthOfYear, dayOfMonth, 18, 0, 0);
                        }else{
                            c2.set(year, monthOfYear, dayOfMonth);
                        }
                        tglMasukFinal = dateFormat.format(c2.getTime());
                        inTglMulaiJadwal.setText(tglMasukFinal);
                    }
                }, c_mulai.get(Calendar.YEAR), c_mulai.get(Calendar.MONTH), c_mulai.get(Calendar.DAY_OF_MONTH));
                d.show();
            }
        });
        inTglSelesaiJadwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog d = new DatePickerDialog(InputJadwalActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                        tugasFinal = listTugasJadwal.getSelectedItem().toString();
                        System.out.println("TUGAS = "+tugasFinal);
                        Calendar c2 = Calendar.getInstance();
                        if(tugasFinal.equalsIgnoreCase("Satpam Pagi")){
                            c2.set(year, monthOfYear, dayOfMonth, 18, 0, 0);
                        }else if(tugasFinal.equalsIgnoreCase("Satpam Malam")){
                            c2.set(year, monthOfYear, dayOfMonth, 6, 0, 0);
                        }else{
                            c2.set(year, monthOfYear, dayOfMonth);
                        }
                        tglSelesaiFinal = dateFormat.format(c2.getTime());
                        inTglSelesaiJadwal.setText(tglSelesaiFinal);
                    }
                }, c_selesai.get(Calendar.YEAR), c_selesai.get(Calendar.MONTH), c_selesai.get(Calendar.DAY_OF_MONTH));
                d.show();
            }
        });
        bInputJadwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usernameFinal = listUsername.get(listUsernameJadwal.getSelectedItemPosition());
                System.out.println("TUGAS = "+tugasFinal);
                System.out.println("MULAI = "+tglMasukFinal);

                aksi = "set_jadwal";
                new ConnectionHelper().execute();
            }
        });

        aksi = "get_username";
    }

    private void onPilihJadwal(String jadwal){
        if(jadwal.equalsIgnoreCase("SATPAM")){
            dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm");
        }else{
            dateFormat = new SimpleDateFormat("dd MMM yyyy");
        }

        if(jadwal.equalsIgnoreCase("SATPAM")){
            listTugasJadwal.setVisibility(View.VISIBLE);
            listTugasJadwalCaraka.setVisibility(View.GONE);
            tugasFinal = listTugasJadwal.getSelectedItem().toString();
        }else{
            listTugasJadwal.setVisibility(View.GONE);
            listTugasJadwalCaraka.setVisibility(View.VISIBLE);
            tugasFinal = listTugasJadwalCaraka.getSelectedItem().toString();
        }
    }

    private void onPenugasanClick(String penugasan){

        tugasFinal = penugasan;
        if(tugasFinal.equalsIgnoreCase("Satpam Pagi")){
            c_mulai.set(Calendar.HOUR, 6);
            c_mulai.set(Calendar.SECOND, 0);
            c_mulai.set(Calendar.MINUTE, 0);
            c_selesai.set(Calendar.HOUR, 18);
            c_selesai.set(Calendar.MINUTE, 0);
            c_selesai.set(Calendar.SECOND, 0);
        }else if(tugasFinal.equalsIgnoreCase("Satpam Malam")){
            c_mulai.set(Calendar.HOUR, 18);
            c_mulai.set(Calendar.SECOND, 0);
            c_mulai.set(Calendar.MINUTE, 0);
            c_selesai.set(Calendar.HOUR, 6);
            c_selesai.set(Calendar.MINUTE, 0);
            c_selesai.set(Calendar.SECOND, 0);
        }else{
            c_mulai.set(Calendar.HOUR, 7);
            c_mulai.set(Calendar.SECOND, 0);
            c_mulai.set(Calendar.MINUTE, 0);
            c_selesai.set(Calendar.HOUR, 12);
            c_selesai.set(Calendar.MINUTE, 0);
            c_selesai.set(Calendar.SECOND, 0);
        }
        tglMasukFinal = dateFormat.format(c_mulai.getTime());
        tglSelesaiFinal = dateFormat.format(c_selesai.getTime());
        inTglMulaiJadwal.setText(tglMasukFinal);
        inTglSelesaiJadwal.setText(tglSelesaiFinal);
    }

    private class ConnectionHelper extends AsyncTask<String, Void, String> {
        ProgressDialog p = new ProgressDialog(InputJadwalActivity.this);
        @Override
        protected void onPreExecute() {
            response = "";
            p.setTitle("Tunggu sebentar...");
            p.show();
        }

        @Override
        protected String doInBackground(String... params) {
            URL url = null;
            try {
                List<Pair> paramss = new ArrayList<Pair>();
                if (aksi.equals("get_username") && pilih.equalsIgnoreCase("SATPAM")) {
                    url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=getAllPegawaiSatpam");
                }else if (aksi.equals("get_username") && pilih.equalsIgnoreCase("CARAKA")) {
                    url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=getAllPegawaiCaraka");
                }
                else {
                    url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=setJadwal");
                    String mulai = tglMasukFinal;
                    String selesai = tglSelesaiFinal;
                    SimpleDateFormat myFormat;
                    SimpleDateFormat fromDB = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    if(pilih.equalsIgnoreCase("SATPAM")){
                        myFormat = new SimpleDateFormat("dd MMM yy HH:mm");
                    }else{
                        myFormat = new SimpleDateFormat("dd MMM yy");
                    }

                    try {
                        mulai = fromDB.format(myFormat.parse(tglMasukFinal));
                        selesai = fromDB.format(myFormat.parse(tglSelesaiFinal));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    paramss.add(new Pair("tglmulai", mulai));
                    paramss.add(new Pair("tglselesai", selesai));
                    paramss.add(new Pair("username", usernameFinal));
                    paramss.add(new Pair("tugas", tugasFinal));
                }

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getQuery(paramss));
                writer.flush();
                writer.close();
                os.close();

                conn.connect();
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line=br.readLine()) != null) {
                        response+=line;
                    }
                } else {
                    response = "";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            p.cancel();
            if (result != "") {
                Log.d("Ar-Rafi Log", result.replace("null", ""));
                try {
                    if (aksi.equals("get_username")) {
                        JSONArray json = new JSONArray(result.replace("null",""));
                        listUsername.clear();
                        listNama.clear();
                        for(int i = 0;i < json.length(); i++) {
                            listUsername.add(json.getJSONObject(i).getString("username"));
                            listNama.add(json.getJSONObject(i).getString("nama"));
                        }
                        listUsernameAdapter = new ArrayAdapter<String>(InputJadwalActivity.this, android.R.layout.simple_spinner_item, listNama);
                        listUsernameJadwal.setAdapter(listUsernameAdapter);
                    } else {
                        JSONObject json = new JSONObject(result.replace("null", ""));
                        if (json.getString("success").equals("1")) {
                            Toast.makeText(InputJadwalActivity.this, "Input berhasil", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(InputJadwalActivity.this, "Error", Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(InputJadwalActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(InputJadwalActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getQuery(List<Pair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Pair<String, String> pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.first, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.second, "UTF-8"));
        }

        return result.toString();
    }

}
