package cukil.ar_rafi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by syauqiilham on 6/17/16.
 */
public class ContentAlat extends Fragment{
    ListView listAlat;
    String response, tglPeristiwa;
    ArrayList<String> jadwalList = new ArrayList<String>();
    AdapterAlat adapterAlat;
    TextView tNoData;
    SimpleDateFormat dateFormat;
    String username, status;
    Spinner listBulanPeriode, listTahunPeriode;
    String bulan, tahun, pilih, urlStr;
    Spinner spinner;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_alat,container,false);
        listAlat = (ListView) v.findViewById(R.id.listAlat);
        tNoData = (TextView) v.findViewById(R.id.tNoData);
        spinner = (Spinner) v.findViewById(R.id.spinner);
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final Calendar c = Calendar.getInstance();
        tglPeristiwa = dateFormat.format(c.getTime());

        FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.fab);
        final Bundle b = this.getArguments();
        status = b.getString("status");
        if (!status.equals("katu")) {
            username = b.getString("username");
            urlStr = "http://" + getResources().getString(R.string.ipaddr) + "/arrafibr/api.php?act=getAllAlatByPeriodeAndUsername&username="+username;
            System.out.println("URL = username");
        }
        else {
            username = "";
            urlStr = "http://" + getResources().getString(R.string.ipaddr) + "/arrafibr/api.php?act=getAllAlatByPeriode";
            fab.setVisibility(View.GONE);
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentInputAlat fragmentInputAlat = new ContentInputAlat();
                Bundle bInputAlat = new Bundle();
                bInputAlat.putString("username", username);
                bInputAlat.putString("status", "caraka");
                fragmentInputAlat.setArguments(bInputAlat);
                android.support.v4.app.FragmentTransaction fragmentTransactionInputAlat = getFragmentManager().beginTransaction();
                fragmentTransactionInputAlat.replace(R.id.frame, fragmentInputAlat);
                fragmentTransactionInputAlat.commit();
            }
        });

        bulan = new SimpleDateFormat("MM").format(c.getTime());
        tahun = new SimpleDateFormat("yyyy").format(c.getTime());
        listBulanPeriode = (Spinner) v.findViewById(R.id.listBulanPeriode);
        listTahunPeriode = (Spinner) v.findViewById(R.id.listTahunPeriode);

        listBulanPeriode.setSelection((Integer.parseInt(bulan) - 1));
        listTahunPeriode.setSelection(1);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                pilih = adapterView.getSelectedItem().toString();
                if (!status.equals("katu")) {
                    if(pilih.equalsIgnoreCase("All")){
                        urlStr = "http://" + getResources().getString(R.string.ipaddr) + "/arrafibr/api.php?act=getAllAlatByPeriodeAndUsername&username="+username;
                    }else{
                        urlStr = "http://" + getResources().getString(R.string.ipaddr) + "/arrafibr/api.php?act=getAllAlatByPeriodeAndUsernameAndProgress&username="+username+"&progress="+pilih.toLowerCase();
                    }
                }else{
                    if(pilih.equalsIgnoreCase("All")){
                        urlStr = "http://" + getResources().getString(R.string.ipaddr) + "/arrafibr/api.php?act=getAllAlatByPeriode";
                    }else{
                        urlStr = "http://" + getResources().getString(R.string.ipaddr) + "/arrafibr/api.php?act=getAllAlatByPeriodeAndProgress&progress="+pilih.toLowerCase();
                    }
                }
                new ConnectionHelper().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        listBulanPeriode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bulan = String.valueOf(position + 1);
                new ConnectionHelper().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        listTahunPeriode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tahun = listTahunPeriode.getSelectedItem().toString();
                new ConnectionHelper().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        new ConnectionHelper().execute();
    }

    private class ConnectionHelper extends AsyncTask<String, Void, String> {
        String username, password;
        ProgressDialog p = new ProgressDialog(getActivity());
        @Override
        protected void onPreExecute() {
            response = "";
            p.setTitle("Tunggu sebentar...");
            p.show();
        }

        @Override
        protected String doInBackground(String... params) {
            URL url = null;
            try {
//                if(pilih.equalsIgnoreCase("All")){
//                    url = new URL("http://" + getResources().getString(R.string.ipaddr) + "/arrafibr/api.php?act=getAllAlatByPeriode");
//                }else{
//                    url = new URL("http://" + getResources().getString(R.string.ipaddr) + "/arrafibr/api.php?act=getAllAlatByPeriodeAndProgress&progress="+pilih.toLowerCase());
//                }
                url = new URL(urlStr);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                List<Pair> paramss = new ArrayList<Pair>();
                paramss.add(new Pair("bln", bulan));
                paramss.add(new Pair("thn", tahun));

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getQuery(paramss));
                writer.flush();
                writer.close();
                os.close();

                conn.connect();
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line=br.readLine()) != null) {
                        response+=line;
                    }
                } else {
                    response = "";
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            p.cancel();
            if (result != "") {
                Log.d("Ar-Rafi Log", result.replace("null",""));
                try {
                    JSONArray json = new JSONArray(result.replace("null",""));
                    adapterAlat = new AdapterAlat(getActivity(), json);
                    listAlat.setAdapter(adapterAlat);
                    if (json.length() == 0)
                        tNoData.setVisibility(View.VISIBLE);
                    else
                        tNoData.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Tidak ada data", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getQuery(List<Pair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Pair<String, String> pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.first, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.second, "UTF-8"));
        }

        return result.toString();
    }
}
