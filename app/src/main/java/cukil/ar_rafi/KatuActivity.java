package cukil.ar_rafi;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class KatuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    String username = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_katu);
        Bundle be = getIntent().getExtras();
        username = be.getString("username");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setTitle("Ar-Rafi - Beranda");
        ContentBerandaKatu fragment = new ContentBerandaKatu();
        Bundle b = new Bundle();
        b.putInt("layout", R.layout.content_beranda_katu);
        fragment.setArguments(b);
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.katu, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        switch(item.getItemId()) {
            case R.id.nav_beranda:
                setTitle("Ar-Rafi - Beranda");
                ContentBerandaKatu fragmentBeranda = new ContentBerandaKatu();
                Bundle bBeranda = new Bundle();
                bBeranda.putString("username", username);
                bBeranda.putString("status", "katu");
                fragmentBeranda.setArguments(bBeranda);
                android.support.v4.app.FragmentTransaction fragmentTransactionBeranda = getSupportFragmentManager().beginTransaction();
                fragmentTransactionBeranda.replace(R.id.frame, fragmentBeranda);
                fragmentTransactionBeranda.commit();
                return true;
            case R.id.nav_jadwal:
                setTitle("Ar-Rafi - Jadwal");
                ContentJadwal fragmentJadwal = new ContentJadwal();
                Bundle bJadwal = new Bundle();
                bJadwal.putString("username", username);
                bJadwal.putString("status", "katu");
                fragmentJadwal.setArguments(bJadwal);
                android.support.v4.app.FragmentTransaction fragmentTransactionJadwal = getSupportFragmentManager().beginTransaction();
                fragmentTransactionJadwal.replace(R.id.frame, fragmentJadwal);
                fragmentTransactionJadwal.commit();
                return true;
            case R.id.nav_presensi:
                setTitle("Ar-Rafi - Presensi");
                ContentPresensi fragmentPresensi = new ContentPresensi();
                Bundle bPresensi = new Bundle();
                bPresensi.putString("username", username);
                bPresensi.putString("status", "katu");
                fragmentPresensi.setArguments(bPresensi);
                android.support.v4.app.FragmentTransaction fragmentTransactionPresensi = getSupportFragmentManager().beginTransaction();
                fragmentTransactionPresensi.replace(R.id.frame, fragmentPresensi);
                fragmentTransactionPresensi.commit();
                return true;
            case R.id.nav_kunjungan:
                setTitle("Ar-Rafi - Kunjungan");
                ContentKunjungan fragmentKunjungan = new ContentKunjungan();
                Bundle bKunjungan = new Bundle();
                bKunjungan.putString("username", username);
                bKunjungan.putString("status", "katu");
                fragmentKunjungan.setArguments(bKunjungan);
                android.support.v4.app.FragmentTransaction fragmentTransactionKunjungan = getSupportFragmentManager().beginTransaction();
                fragmentTransactionKunjungan.replace(R.id.frame, fragmentKunjungan);
                fragmentTransactionKunjungan.commit();
                return true;
            case R.id.nav_kejadian:
                setTitle("Ar-Rafi - Kejadian");
                ContentPeristiwa fragmentPeristiwa = new ContentPeristiwa();
                Bundle bPeristiwa = new Bundle();
                bPeristiwa.putString("username", username);
                bPeristiwa.putString("status", "katu");
                fragmentPeristiwa.setArguments(bPeristiwa);
                android.support.v4.app.FragmentTransaction fragmentTransactionPeristiwa = getSupportFragmentManager().beginTransaction();
                fragmentTransactionPeristiwa.replace(R.id.frame, fragmentPeristiwa);
                fragmentTransactionPeristiwa.commit();
                return true;
            case R.id.nav_kebersihan:
                setTitle("Ar-Rafi - Kebersihan");
                ContentKebersihan fragmentKebersihan = new ContentKebersihan();
                Bundle bKebersihan = new Bundle();
                bKebersihan.putString("username", username);
                bKebersihan.putString("status", "katu");
                fragmentKebersihan.setArguments(bKebersihan);
                android.support.v4.app.FragmentTransaction fragmentTransactionKebersihan = getSupportFragmentManager().beginTransaction();
                fragmentTransactionKebersihan.replace(R.id.frame, fragmentKebersihan);
                fragmentTransactionKebersihan.commit();
                return true;
            case R.id.nav_alat_kebersihan:
                setTitle("Ar-Rafi - Pengajuan Alat Kebersihan");
                ContentAlat fragmentAlat = new ContentAlat();
                Bundle bAlat = new Bundle();
                bAlat.putString("username", username);
                bAlat.putString("status", "katu");
                fragmentAlat.setArguments(bAlat);
                android.support.v4.app.FragmentTransaction fragmentTransactionAlat = getSupportFragmentManager().beginTransaction();
                fragmentTransactionAlat.replace(R.id.frame, fragmentAlat);
                fragmentTransactionAlat.commit();
                return true;
            case R.id.nav_penilaian:
                setTitle("Ar-Rafi - Penilaian");
                ContentPenilaian fragmentPenilaian = new ContentPenilaian();
                Bundle bPenilaian = new Bundle();
                bPenilaian.putString("username", username);
                bPenilaian.putString("status", "katu");
                fragmentPenilaian.setArguments(bPenilaian);
                android.support.v4.app.FragmentTransaction fragmentTransactionPenilaian = getSupportFragmentManager().beginTransaction();
                fragmentTransactionPenilaian.replace(R.id.frame, fragmentPenilaian);
                fragmentTransactionPenilaian.commit();
                return true;
            case R.id.nav_logout:
                Intent i = new Intent(KatuActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
                return true;
            default:
                return true;
        }

    }
}
