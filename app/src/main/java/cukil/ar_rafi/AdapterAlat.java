package cukil.ar_rafi;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by syauqiilham on 6/18/16.
 */
public class AdapterAlat extends BaseAdapter implements ListAdapter {
    private final Activity activity;
    private final JSONArray jsonArray;

    public AdapterAlat(Activity activity, JSONArray jsonArray) {
        assert activity != null;
        assert jsonArray != null;
        this.jsonArray = jsonArray;
        this.activity = activity;
    }
    @Override
    public int getCount() {
        if (jsonArray == null)
            return 0;
        else
            return jsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        if (jsonArray == null)
            return null;
        else
            return jsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        JSONObject jsonObject = getItem(position);
        return jsonObject.optInt("id_peristiwa");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(R.layout.list_alat, null);
            holder = new ViewHolder();
            holder.tNama = (TextView) convertView.findViewById(R.id.tNama);
            holder.tTgl = (TextView) convertView.findViewById(R.id.tTglAlat);
            holder.tBarang = (TextView) convertView.findViewById(R.id.tBarang);
            holder.tMerk = (TextView) convertView.findViewById(R.id.tMerk);
            holder.tJumlah = (TextView) convertView.findViewById(R.id.tJumlah);
            holder.tKet = (TextView) convertView.findViewById(R.id.tKet);
            holder.tProgress = (TextView) convertView.findViewById(R.id.tProgress);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        JSONObject jsonObject = getItem(position);
        if (jsonObject != null) {
            holder.tNama.setText(jsonObject.optString("username"));
            holder.tTgl.setText(jsonObject.optString("tgl_pengajuan"));
            holder.tBarang.setText(jsonObject.optString("nama_barang"));
            holder.tMerk.setText(jsonObject.optString("merk_barang"));
            holder.tJumlah.setText(jsonObject.optString("jumlah_permintaan"));
            holder.tKet.setText(jsonObject.optString("ket_permintaan"));
            holder.tProgress.setText(jsonObject.optString("progress"));
        }

        return convertView;
    }

    class ViewHolder {
        TextView tNama;
        TextView tTgl;
        TextView tBarang;
        TextView tMerk;
        TextView tJumlah;
        TextView tKet;
        TextView tProgress;
    }
}

