package cukil.ar_rafi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by syauqiilham on 6/18/16.
 */
public class AdapterJadwal extends BaseAdapter implements ListAdapter {
    private final Activity activity;
    private final JSONArray jsonArray;
    private final String jadwal, username, status;

    public AdapterJadwal(Activity activity, JSONArray jsonArray, String jadwal, String username, String status) {
        assert activity != null;
        assert jsonArray != null;
        assert jadwal != null;
        this.jsonArray = jsonArray;
        this.activity = activity;
        this.jadwal = jadwal;
        this.username = username;
        this.status = status;
    }
    @Override
    public int getCount() {
        if (jsonArray == null)
            return 0;
        else
            return jsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        if (jsonArray == null)
            return null;
        else
            return jsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        JSONObject jsonObject = getItem(position);
        return jsonObject.optInt("id_jadwal");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(R.layout.list_jadwal, null);
            holder = new ViewHolder();
            holder.tNama = (TextView) convertView.findViewById(R.id.tNama);
            holder.tTglMulai = (TextView) convertView.findViewById(R.id.tTglMulai);
            holder.tTglSelesai = (TextView) convertView.findViewById(R.id.tTglSelesai);
            holder.tTugasDi = (TextView) convertView.findViewById(R.id.tTugasDi);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final JSONObject jsonObject = getItem(position);
        if (jsonObject != null) {
            String mulai = jsonObject.optString("tgl_mulai");
            String selesai = jsonObject.optString("tgl_selesai");
            SimpleDateFormat myFormat;
            SimpleDateFormat fromDB = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            if(jadwal.equalsIgnoreCase("SATPAM")){
                myFormat = new SimpleDateFormat("dd MMM yy HH:mm");
            }else{
                myFormat = new SimpleDateFormat("dd MMM yy");
            }

            try {
                mulai = myFormat.format(fromDB.parse(jsonObject.optString("tgl_mulai")));
                selesai = myFormat.format(fromDB.parse(jsonObject.optString("tgl_selesai")));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            holder.tNama.setText(jsonObject.optString("nama"));
            holder.tTglMulai.setText(mulai);
            holder.tTglSelesai.setText(selesai);
            holder.tTugasDi.setText(jsonObject.optString("tugas"));
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");

                    try {

                        String dateInString = myFormat.format(fromUser.parse(jsonObject.optString("tgl_mulai")));
                        Date date = myFormat.parse(dateInString);
//                        System.out.println("TGL SEKARANG = "+myFormat.format(new Date()));
//                        System.out.println("TGL MULAI = "+myFormat.format(date));
//                        System.out.println("COMPARE = "+date.equals(new Date()));
//                        System.out.println("COMPARE Str= "+myFormat.format(new Date()).equals(myFormat.format(date)));
                        if(myFormat.format(new Date()).equals(myFormat.format(date))){
                            Intent i = new Intent(activity, InputPresensiActivity.class);
                            Bundle bi = new Bundle();
                            bi.putString("username", username);
                            bi.putString("status", status);
                            i.putExtras(bi);
                            activity.startActivity(i);
                        }else{
                            Toast.makeText(activity, "Pilih jadwal hari ini untuk input presensi!", Toast.LENGTH_LONG).show();
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        return convertView;
    }

    class ViewHolder {
        TextView tNama;
        TextView tTglMulai;
        TextView tTglSelesai;
        TextView tTugasDi;
    }
}

