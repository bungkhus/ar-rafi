package cukil.ar_rafi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by syauqiilham on 6/18/16.
 */
public class AdapterInputKebersihan extends BaseAdapter implements ListAdapter {
    private final Activity activity;
    private final JSONArray jsonArray;
    String response, idPresensi, waktuFinal, idNamaFinal, statusFinal, keterangan;
    SimpleDateFormat dateFormat;

    public AdapterInputKebersihan(Activity activity, JSONArray jsonArray, String idPresensi) {
        assert activity != null;
        assert jsonArray != null;
        this.jsonArray = jsonArray;
        this.activity = activity;
        this.idPresensi = idPresensi;
    }
    @Override
    public int getCount() {
        if (jsonArray == null)
            return 0;
        else
            return jsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        if (jsonArray == null)
            return null;
        else
            return jsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        JSONObject jsonObject = getItem(position);
        return jsonObject.optInt("id_tugas_caraka");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(R.layout.list_kegiatan_kebersihan, null);
            holder = new ViewHolder();
            holder.tKegiatan = (TextView) convertView.findViewById(R.id.tKegiatan);
            holder.tKet = (EditText) convertView.findViewById(R.id.tKet);
            holder.tKegiatanStatus = (RadioButton) convertView.findViewById(R.id.tKegiatanStatus);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final JSONObject jsonObject = getItem(position);
        if (jsonObject != null) {
            switch (jsonObject.optString("status")) {
                case "1":
                    holder.tKegiatanStatus.setChecked(true);
                    break;
                case "":
                    holder.tKegiatanStatus.setChecked(false);
                    break;
                default:
                    break;
            }
            holder.tKegiatan.setText(jsonObject.optString("nama_kegiatan"));
            holder.tKegiatanStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    idNamaFinal = jsonObject.optString("id_nama");
                    if (isChecked) statusFinal = "1";
                    else statusFinal = "0";
                    dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    final Calendar c = Calendar.getInstance();
                    waktuFinal = dateFormat.format(c.getTime());
                    keterangan = holder.tKet.getText().toString();
                    new ConnectionHelper().execute();
                }
            });
        }

        return convertView;
    }

    private class ConnectionHelper extends AsyncTask<String, Void, String> {
        String username, password;
        ProgressDialog p = new ProgressDialog(activity);
        @Override
        protected void onPreExecute() {
            response = "";
            p.setTitle("Tunggu sebentar...");
            p.show();
        }

        @Override
        protected String doInBackground(String... params) {
            URL url = null;
            try {
//                url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=getAllPeristiwaByDate&tglPeristiwa="+tglPeristiwa);
                url = new URL("http://"+activity.getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=setKegKebersihan");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                List<Pair> paramss = new ArrayList<Pair>();
                paramss.add(new Pair("id_presensi", idPresensi));
                paramss.add(new Pair("id_nama", idNamaFinal));
                paramss.add(new Pair("waktu", waktuFinal));
                paramss.add(new Pair("status", statusFinal));
                paramss.add(new Pair("ket", keterangan));

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getQuery(paramss));
                writer.flush();
                writer.close();
                os.close();

                conn.connect();
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line=br.readLine()) != null) {
                        response+=line;
                    }
                } else {
                    response = "";
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            p.cancel();
            if (result != "") {
                Log.d("Ar-Rafi Log", result.replace("null", ""));
                try {
                    JSONObject json = new JSONObject(result.replace("null", ""));
                    if (json.getString("success").equals("1")) {
                        Toast.makeText(activity, "Input berhasil", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getQuery(List<Pair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Pair<String, String> pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.first, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.second, "UTF-8"));
        }

        return result.toString();
    }

    class ViewHolder {
        TextView tKegiatan;
        RadioButton tKegiatanStatus;
        EditText tKet;
    }
}

