package cukil.ar_rafi;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class InputKunjunganActivity extends AppCompatActivity {

    EditText inTglKunjungan, inJamKunjungan, inNamaKunjungan, inInstitusiKunjungan, inAgendaKunjungan, inJumlahRombongan;
    Button bInputKunjungan;
    String tglFinal, jamFinal, namaFinal, instisusiFinal, agendaFinal, idPresensi, jumlahRombongan;
    SimpleDateFormat dateFormat, dateFormat2;
    String response, aksi = "getIdPresensi", username, status;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_kunjungan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        inTglKunjungan = (EditText) findViewById(R.id.inTglKunjungan);
        inJamKunjungan = (EditText) findViewById(R.id.inJamKunjungan);
        inNamaKunjungan = (EditText) findViewById(R.id.inNamaKunjungan);
        inInstitusiKunjungan = (EditText) findViewById(R.id.inInstitusiKunjungan);
        inAgendaKunjungan = (EditText) findViewById(R.id.inAgendaKunjungan);
        inJumlahRombongan = (EditText) findViewById(R.id.inJumlahRombongan);
        bInputKunjungan = (Button) findViewById(R.id.bInputKunjungan);
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat2 = new SimpleDateFormat("HH:mm");
        final Calendar c = Calendar.getInstance();
        tglFinal = dateFormat.format(c.getTime());
        jamFinal = dateFormat2.format(c.getTime());
        inTglKunjungan.setText(tglFinal);
        inJamKunjungan.setText(jamFinal);

        inTglKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog d = new DatePickerDialog(InputKunjunganActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar c2 = Calendar.getInstance();
                        c2.set(year, monthOfYear, dayOfMonth);
                        tglFinal = dateFormat.format(c2.getTime());
                        inTglKunjungan.setText(tglFinal);
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                d.show();
            }
        });
        inJamKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog t = new TimePickerDialog(InputKunjunganActivity.this, new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        jamFinal = hourOfDay + ":" + minute;
                        inJamKunjungan.setText(jamFinal);
                    }
                }, c.get(Calendar.HOUR), c.get(Calendar.MINUTE), true);
                t.show();
            }
        });
        bInputKunjungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                namaFinal = inNamaKunjungan.getText().toString();
                instisusiFinal = inInstitusiKunjungan.getText().toString();
                agendaFinal = inAgendaKunjungan.getText().toString();
                jumlahRombongan = inJumlahRombongan.getText().toString();

                aksi = "setKunjungan";
                new ConnectionHelper().execute();
            }
        });
        Bundle b = getIntent().getExtras();
        username = b.getString("username");
        status = b.getString("status");

        aksi = "getIdPresensi";
        new ConnectionHelper().execute();
    }

    private class ConnectionHelper extends AsyncTask<String, Void, String> {
        ProgressDialog p = new ProgressDialog(InputKunjunganActivity.this);
        @Override
        protected void onPreExecute() {
            response = "";
            p.setTitle("Tunggu sebentar...");
            p.show();
        }

        @Override
        protected String doInBackground(String... params) {
            URL url = null;
            try {
                if (aksi.equals("getIdPresensi"))
                    url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=getPresensiByUsername");
                else
                    url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=setKunjungan");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                List<Pair> paramss = new ArrayList<Pair>();
                if (aksi.equals("getIdPresensi")) {
                    paramss.add(new Pair("username", username));
                } else {
                    paramss.add(new Pair("tglKunjungan", tglFinal));
                    paramss.add(new Pair("jamKunjungan", jamFinal));
                    paramss.add(new Pair("nama", namaFinal));
                    paramss.add(new Pair("institusi", instisusiFinal));
                    paramss.add(new Pair("agenda", agendaFinal));
                    paramss.add(new Pair("idPresensi", idPresensi));
                    paramss.add(new Pair("jumlah_rombongan", jumlahRombongan));
                }
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getQuery(paramss));
                writer.flush();
                writer.close();
                os.close();

                conn.connect();
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line=br.readLine()) != null) {
                        response+=line;
                    }
                } else {
                    response = "";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            p.cancel();
            if (result != "") {
                Log.d("Ar-Rafi Log", result.replace("null", ""));
                try {
                    if (aksi.equals("getIdPresensi")) {
                        JSONArray json = new JSONArray(result.replace("null",""));
                        for (int i = 0; i < json.length(); i++) {
                            JSONObject row = json.getJSONObject(i);
                            idPresensi = row.getString("id_presensi");
                        }
                    } else {
                        JSONObject json = new JSONObject(result.replace("null", ""));
                        if (json.getString("success").equals("1")) {
                            Toast.makeText(InputKunjunganActivity.this, "Input berhasil", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(InputKunjunganActivity.this, "Error", Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(InputKunjunganActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(InputKunjunganActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getQuery(List<Pair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Pair<String, String> pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.first, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.second, "UTF-8"));
        }

        return result.toString();
    }
}
