package cukil.ar_rafi;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bungkhus on 7/30/16.
 */
public class TabJadwalKatu extends Fragment {

    private View rootView;
    private static final String TEXT_FRAGMENT = "TEXT_FRAGMENT";
    private final static String TAG = "PembimbingTabbedKelompokFragment";
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Toolbar mToolbar;
    private String username;

    public TabJadwalKatu(){}


    public static TabJadwalKatu newInstance(String text){
        TabJadwalKatu mFragment = new TabJadwalKatu();
        Bundle mBundle = new Bundle();
        mBundle.putString(TEXT_FRAGMENT, text);
        mFragment.setArguments(mBundle);
        return mFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.tabbed_layout, container, false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("JADWAL");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setElevation(0);
        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        mToolbar.setVisibility(View.GONE);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewPager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        final Bundle b = this.getArguments();
        username = b.getString("username");

        return rootView;
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        ContentJadwal fragmentJadwal = new ContentJadwal();
        Bundle bJadwal = new Bundle();
        bJadwal.putString("username", username);
        bJadwal.putString("status", "katu");
        fragmentJadwal.setArguments(bJadwal);

        adapter.addFragment(fragmentJadwal, "SATPAM");
        adapter.addFragment(fragmentJadwal, "CARAKA");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        System.out.println("iki start");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(false);
    }

}
