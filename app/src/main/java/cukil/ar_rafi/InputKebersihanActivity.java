package cukil.ar_rafi;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class InputKebersihanActivity extends AppCompatActivity {

    ListView listKegKebersihan;
    String response, aksi, idPresensi, username, status;
    AdapterInputKebersihan adapterKebersihan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_kebersihan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        listKegKebersihan = (ListView) findViewById(R.id.listKegKebersihan);

        Bundle b = getIntent().getExtras();
        username = b.getString("username");
        status = b.getString("status");

        aksi = "getIdPresensi";
        new ConnectionHelper().execute();
    }

    private class ConnectionHelper extends AsyncTask<String, Void, String> {
        ProgressDialog p = new ProgressDialog(InputKebersihanActivity.this);
        @Override
        protected void onPreExecute() {
            response = "";
            p.setTitle("Tunggu sebentar...");
            p.show();
        }

        @Override
        protected String doInBackground(String... params) {
            URL url = null;
            try {
                List<Pair> paramss = new ArrayList<Pair>();
                if (aksi.equals("getIdPresensi")) {
                    url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=getPresensiByUsername&username="+username);
                    paramss.add(new Pair("username", username));
                }
                else {
                    url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=getAllKegKebersihan&id_presensi="+idPresensi);
                    paramss.add(new Pair("id_presensi", idPresensi));
                }

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getQuery(paramss));
                writer.flush();
                writer.close();
                os.close();

                conn.connect();
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line=br.readLine()) != null) {
                        response+=line;
                    }
                } else {
                    response = "";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            p.cancel();
            if (result != "") {
                Log.d("Ar-Rafi Log", result.replace("null", ""));
                try {
                    if (aksi.equals("getIdPresensi")) {
                        JSONArray json = new JSONArray(result.replace("null",""));
                        for (int i = 0; i < json.length(); i++) {
                            JSONObject row = json.getJSONObject(i);
                            idPresensi = row.getString("id_presensi");
                        }
                        aksi = "getKegKeb";
                        new ConnectionHelper().execute();
                    } else {
                        JSONArray json = new JSONArray(result);
                        adapterKebersihan = new AdapterInputKebersihan(InputKebersihanActivity.this, json, idPresensi);
                        listKegKebersihan.setAdapter(adapterKebersihan);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(InputKebersihanActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(InputKebersihanActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getQuery(List<Pair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Pair<String, String> pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.first, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.second, "UTF-8"));
        }

        return result.toString();
    }

}
