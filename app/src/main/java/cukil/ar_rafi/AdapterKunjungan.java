package cukil.ar_rafi;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by syauqiilham on 6/18/16.
 */
public class AdapterKunjungan extends BaseAdapter implements ListAdapter {
    private final Activity activity;
    private final JSONArray jsonArray;

    public AdapterKunjungan(Activity activity, JSONArray jsonArray) {
        assert activity != null;
        assert jsonArray != null;
        this.jsonArray = jsonArray;
        this.activity = activity;
    }
    @Override
    public int getCount() {
        if (jsonArray == null)
            return 0;
        else
            return jsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        if (jsonArray == null)
            return null;
        else
            return jsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        JSONObject jsonObject = getItem(position);
        return jsonObject.optInt("id_kunjungan");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(R.layout.list_kunjungan, null);
            holder = new ViewHolder();
            holder.tTglKunjungan = (TextView) convertView.findViewById(R.id.tTglKunjungan);
            holder.tJamKunjungan = (TextView) convertView.findViewById(R.id.tJamKunjungan);
            holder.tNama = (TextView) convertView.findViewById(R.id.tNama);
            holder.tInstitusi = (TextView) convertView.findViewById(R.id.tInstitusi);
            holder.tAgenda = (TextView) convertView.findViewById(R.id.tAgenda);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        JSONObject jsonObject = getItem(position);
        if (jsonObject != null) {
            String tgl = jsonObject.optString("tgl_kunjungan");
            SimpleDateFormat myFormat = new SimpleDateFormat("dd MMM yy");
            SimpleDateFormat fromDB = new SimpleDateFormat("yyyy-MM-dd");

            try {
                tgl  = myFormat.format(fromDB.parse(tgl));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.tTglKunjungan.setText(tgl);
            holder.tJamKunjungan.setText(jsonObject.optString("jam_kunjungan"));
            holder.tNama.setText(jsonObject.optString("nama")+" ( "+jsonObject.optString("jumlah_rombongan")+" orang)");
            holder.tInstitusi.setText(jsonObject.optString("institusi"));
            holder.tAgenda.setText(jsonObject.optString("agenda"));
        }

        return convertView;
    }

    class ViewHolder {
        TextView tTglKunjungan;
        TextView tJamKunjungan;
        TextView tNama;
        TextView tInstitusi;
        TextView tAgenda;
    }
}

