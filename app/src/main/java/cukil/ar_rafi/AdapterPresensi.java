package cukil.ar_rafi;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by syauqiilham on 6/18/16.
 */
public class AdapterPresensi extends BaseAdapter implements ListAdapter {
    private final Activity activity;
    private final JSONArray jsonArray;

    public AdapterPresensi(Activity activity, JSONArray jsonArray) {
        assert activity != null;
        assert jsonArray != null;
        this.jsonArray = jsonArray;
        this.activity = activity;
    }
    @Override
    public int getCount() {
        if (jsonArray == null)
            return 0;
        else
            return jsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        if (jsonArray == null)
            return null;
        else
            return jsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        JSONObject jsonObject = getItem(position);
        return jsonObject.optInt("id_presensi");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(R.layout.list_presensi, null);
            holder = new ViewHolder();
            holder.tNamaPegawai = (TextView) convertView.findViewById(R.id.tNamaPegawai);
            holder.tTanggalAbsen = (TextView) convertView.findViewById(R.id.tTanggalAbsen);
            holder.tPresensiHadir = (RadioButton) convertView.findViewById(R.id.tPresensiHadir);
            holder.tPresensiIzin = (RadioButton) convertView.findViewById(R.id.tPresensiIzin);
            holder.tPresensiSakit = (RadioButton) convertView.findViewById(R.id.tPresensiSakit);
            holder.tPresensiAlfa = (RadioButton) convertView.findViewById(R.id.tPresensiAlfa);
            holder.tAlasan = (TextView) convertView.findViewById(R.id.tAlasan);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        JSONObject jsonObject = getItem(position);
        if (jsonObject != null) {
            holder.tNamaPegawai.setText(jsonObject.optString("nama"));
            holder.tTanggalAbsen.setText(jsonObject.optString("tgl_absen"));
            switch (jsonObject.optString("status_presensi")) {
                case "hadir":
                    holder.tPresensiHadir.setChecked(true);
                    break;
                case "izin":
                    holder.tPresensiIzin.setChecked(true);
                    break;
                case "sakit":
                    holder.tPresensiSakit.setChecked(true);
                    break;
                case "alfa":
                    holder.tPresensiAlfa.setChecked(true);
                    break;
                default:
                    break;
            }
            holder.tAlasan.setText(jsonObject.optString("alasan"));
        }

        return convertView;
    }

    static class ViewHolder {
        TextView tNamaPegawai;
        TextView tTanggalAbsen;
        RadioButton tPresensiHadir;
        RadioButton tPresensiIzin;
        RadioButton tPresensiSakit;
        RadioButton tPresensiAlfa;
        TextView tAlasan;
    }
}

