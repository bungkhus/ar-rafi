package cukil.ar_rafi;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by syauqiilham on 6/18/16.
 */
public class AdapterKebersihan extends BaseAdapter implements ListAdapter {
    private final Activity activity;
    private final JSONArray jsonArray;

    public AdapterKebersihan(Activity activity, JSONArray jsonArray) {
        assert activity != null;
        assert jsonArray != null;
        this.jsonArray = jsonArray;
        this.activity = activity;
    }
    @Override
    public int getCount() {
        if (jsonArray == null)
            return 0;
        else
            return jsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        if (jsonArray == null)
            return null;
        else
            return jsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        JSONObject jsonObject = getItem(position);
        return jsonObject.optInt("id_tugas_caraka");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(R.layout.list_kebersihan, null);
            holder = new ViewHolder();
            holder.tNamaPegawai = (TextView) convertView.findViewById(R.id.tNamaPegawai);
            holder.tTanggalKebersihan = (TextView) convertView.findViewById(R.id.tTanggalKebersihan);
            holder.tKegiatan = (TextView) convertView.findViewById(R.id.tKegiatan);
            holder.tKegiatanStatus = (RadioButton) convertView.findViewById(R.id.tKegiatanStatus);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        JSONObject jsonObject = getItem(position);
        if (jsonObject != null) {
            holder.tNamaPegawai.setText(jsonObject.optString("nama"));
            holder.tTanggalKebersihan.setText(jsonObject.optString("waktu"));
            switch (jsonObject.optString("status")) {
                case "1":
                    holder.tKegiatanStatus.setChecked(true);
                    break;
                case "0":
                    holder.tKegiatanStatus.setChecked(false);
                    break;
                default:
                    break;
            }
            holder.tKegiatan.setText(jsonObject.optString("nama_kegiatan"));
        }

        return convertView;
    }

    class ViewHolder {
        TextView tNamaPegawai;
        TextView tTanggalKebersihan;
        TextView tKegiatan;
        RadioButton tKegiatanStatus;
    }
}

