package cukil.ar_rafi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by syauqiilham on 6/17/16.
 */
public class ContentJadwal extends Fragment implements AdapterView.OnItemSelectedListener {
    ListView listJadwal;
    String response;
    ArrayList<String> jadwalList = new ArrayList<String>();
    AdapterJadwal adapterJadwal;
    TextView tNoData;
    String username, status, pilih;
    Spinner spinner;
    ProgressDialog p;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_jadwal,container,false);
        listJadwal = (ListView) v.findViewById(R.id.listJadwal);
        tNoData = (TextView) v.findViewById(R.id.tNoData);
        p = new ProgressDialog(getActivity());
        spinner = (Spinner) v.findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.jadwal_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.fab);
        final Bundle b = this.getArguments();
        status = b.getString("status");
        System.out.println("TEST STATUS = "+status);
        if (!b.getString("status").equals("katu")) {
            fab.setVisibility(View.GONE);
            spinner.setVisibility(View.GONE);
            username = b.getString("username");
            System.out.println("TEST STATUS = "+status);
            System.out.println("TEST UN = "+username);
            fetchData("http://" + getResources().getString(R.string.ipaddr) + "/arrafibr/api.php?act=getAllJadwalByUsername&username="+username);
        }
        else {
            username = "";
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), InputJadwalActivity.class);
                Bundle bi = new Bundle();
                bi.putString("username", username);
                bi.putString("status", b.getString("status"));
                i.putExtras(bi);
                startActivity(i);
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        System.out.println(adapterView.getSelectedItem().toString());
        pilih = adapterView.getSelectedItem().toString();
        if (pilih.equals("SATPAM")) {
            fetchData("http://" + getResources().getString(R.string.ipaddr) + "/arrafibr/api.php?act=getAllJadwalSatpam");
        }else {
            fetchData("http://" + getResources().getString(R.string.ipaddr) + "/arrafibr/api.php?act=getAllJadwalCaraka");
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        System.out.println("SELECT NOTHING");
        pilih = "SATPAM";
    }

    public void fetchData(String url){
        p.setMessage("Loading ...");
        p.show();

        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                p.cancel();
                Log.d("LOG", "Response: " + response.toString());

                try {
                    JSONArray json = new JSONArray(response.replace("null",""));
                    if(!status.equalsIgnoreCase("katu")){
                        JSONObject jsonObject = json.getJSONObject(0);
                        pilih = jsonObject.getString("bagian");
                    }
                    adapterJadwal = new AdapterJadwal(getActivity(), json, pilih, username, status);
                    listJadwal.setAdapter(adapterJadwal);
                    if (json.length() == 0)
                        tNoData.setVisibility(View.VISIBLE);
                    else
                        tNoData.setVisibility(View.GONE);
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                p.cancel();
                Log.e("ERROR", "Submit Error: " + error.getMessage());
                Toast.makeText(getContext(),
                        "Gagal Menampilkan Data.", Toast.LENGTH_LONG).show();
            }
        }) ;

        // Adding request to request queue
        queue.add(strReq);
    }
}
