package cukil.ar_rafi;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class InputPenilaianActivity extends AppCompatActivity {

    Spinner listBulanPeriode, listTahunPeriode, listUsernamePenilaian, spinner, spinnerKesigapan, spinnerPatroli, spinnerLayananTamu, spinnerKeramahan, spinnerKerajinan, spinnerKebersihan;
    EditText inAbsensi;
    TextView tPatroli, tLayananTamu, tKerajinan, tKebersihan;
    Button bInputPenilaian;
    String pilih = "SATPAM";
    String periodeFinal, usernameFinal, absensiFinal, idPegawaiFinal, bulan, tahun,
            kesigapan, patroli, layanan_tamu, keramahan, kerajinan, kebersihan;
    ArrayList<String> listUsername = new ArrayList<String>();
    ArrayList<String> listNama = new ArrayList<String>();
    ArrayList<String> listIdPegawai = new ArrayList<String>();
    String username, status, response, aksi, username_;
    ArrayAdapter<String> listUsernameAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_penilaian);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        listBulanPeriode = (Spinner) findViewById(R.id.listBulanPeriode);
        listTahunPeriode = (Spinner) findViewById(R.id.listTahunPeriode);
        listUsernamePenilaian = (Spinner) findViewById(R.id.listUsernamePenilaian);
        inAbsensi = (EditText) findViewById(R.id.inAbsensi);
        bInputPenilaian = (Button) findViewById(R.id.bInputNilai);
        spinner = (Spinner) findViewById(R.id.spinner);
        spinnerKesigapan = (Spinner) findViewById(R.id.spinnerKesigapan);
        tPatroli = (TextView) findViewById(R.id.tPatroli);
        tLayananTamu = (TextView) findViewById(R.id.tLayananTamu);
        tKerajinan = (TextView) findViewById(R.id.tKerajinan);
        tKebersihan = (TextView) findViewById(R.id.tKebersihan);
        spinnerPatroli = (Spinner) findViewById(R.id.spinnerPatroli);
        spinnerLayananTamu = (Spinner) findViewById(R.id.spinnerLayananTamu);
        spinnerKeramahan = (Spinner) findViewById(R.id.spinnerKeramahan);
        spinnerKerajinan = (Spinner) findViewById(R.id.spinnerKerajinan);
        spinnerKebersihan = (Spinner) findViewById(R.id.spinnerKebersihan);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.jadwal_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                pilih = adapterView.getSelectedItem().toString();
                onPilihJadwal(pilih);

                aksi = "get_username";
                new ConnectionHelper().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        bInputPenilaian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bulan = String.valueOf(listBulanPeriode.getSelectedItemPosition() + 1);
                tahun = listTahunPeriode.getSelectedItem().toString();
                if (bulan.length() == 1) bulan = "0" + bulan;

                kesigapan = String.valueOf((spinnerKesigapan.getSelectedItemPosition()+1)*20);
                patroli = String.valueOf((spinnerPatroli.getSelectedItemPosition()+1)*20);
                layanan_tamu = String.valueOf((spinnerLayananTamu.getSelectedItemPosition()+1)*20);
                keramahan = String.valueOf((spinnerKeramahan.getSelectedItemPosition()+1)*20);
                kerajinan = String.valueOf((spinnerKerajinan.getSelectedItemPosition()+1)*20);
                kebersihan = String.valueOf((spinnerKebersihan.getSelectedItemPosition()+1)*20);
                periodeFinal = tahun + "-" + bulan;
                absensiFinal = inAbsensi.getText().toString();
                idPegawaiFinal = listIdPegawai.get(listUsernamePenilaian.getSelectedItemPosition());

                aksi = "set_penilaian";
                new ConnectionHelper().execute();
            }
        });
        listUsernamePenilaian.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                username_ = listUsernamePenilaian.getSelectedItem().toString();
                aksi = "get_absensi";
                new ConnectionHelper().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        final Calendar c = Calendar.getInstance();
        bulan = new SimpleDateFormat("MM").format(c.getTime());
        tahun = new SimpleDateFormat("yyyy").format(c.getTime());

        listBulanPeriode.setSelection((Integer.parseInt(bulan) - 1));
        listTahunPeriode.setSelection(1);
        Bundle b = getIntent().getExtras();
        username = b.getString("username");
        status = b.getString("status");
    }

    private void onPilihJadwal(String jadwal){
        if(jadwal.equalsIgnoreCase("SATPAM")){
            tPatroli.setVisibility(View.VISIBLE);
            tLayananTamu.setVisibility(View.VISIBLE);
            tKebersihan.setVisibility(View.GONE);
            tKerajinan.setVisibility(View.GONE);
            spinnerPatroli.setVisibility(View.VISIBLE);
            spinnerLayananTamu.setVisibility(View.VISIBLE);
            spinnerKebersihan.setVisibility(View.GONE);
            spinnerKerajinan.setVisibility(View.GONE);
        }else{
            tPatroli.setVisibility(View.GONE);
            tLayananTamu.setVisibility(View.GONE);
            tKebersihan.setVisibility(View.VISIBLE);
            tKerajinan.setVisibility(View.VISIBLE);
            spinnerPatroli.setVisibility(View.GONE);
            spinnerLayananTamu.setVisibility(View.GONE);
            spinnerKebersihan.setVisibility(View.VISIBLE);
            spinnerKerajinan.setVisibility(View.VISIBLE);
        }
    }

    private class ConnectionHelper extends AsyncTask<String, Void, String> {
        ProgressDialog p = new ProgressDialog(InputPenilaianActivity.this);
        @Override
        protected void onPreExecute() {
            response = "";
            p.setTitle("Tunggu sebentar...");
            p.show();
        }

        @Override
        protected String doInBackground(String... params) {
            URL url = null;
            try {
                List<Pair> paramss = new ArrayList<Pair>();
                if (aksi.equals("get_username") && pilih.equalsIgnoreCase("SATPAM")) {
                    url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=getAllPegawaiSatpam");
                }else if (aksi.equals("get_username") && pilih.equalsIgnoreCase("CARAKA")) {
                    url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=getAllPegawaiCaraka");
                } else if (aksi.equals("get_absensi")) {
                    url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=getAbsensiPersenByUsername");
                    paramss.add(new Pair("username", username_));
                }
                else {
//                    url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=setPenilaian&periode="+periodeFinal+"&absensi="+absensiFinal+"&id_pegawai="+idPegawaiFinal+"&kesigapan="+kesigapan+"&patroli="+patroli+"&layanan_tamu="+layanan_tamu+"&keramahan="+keramahan+"&kerajinan="+kerajinan+"&kebersihan="+kebersihan);
                    url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=setPenilaian");
                    double nilai;
                    if(pilih.equalsIgnoreCase("SATPAM")) {
                        nilai = (Double.parseDouble(absensiFinal)+Integer.parseInt(kesigapan)+Integer.parseInt(keramahan)+Integer.parseInt(patroli)+Integer.parseInt(layanan_tamu))/5;
                    }else{
                        nilai = (Double.parseDouble(absensiFinal)+Integer.parseInt(kesigapan)+Integer.parseInt(keramahan)+Integer.parseInt(kebersihan)+Integer.parseInt(kerajinan))/5;
                    }
                    String nilai_akhir = String.valueOf(nilai);
                    paramss.add(new Pair("periode", periodeFinal));
                    paramss.add(new Pair("absensi", absensiFinal));
                    paramss.add(new Pair("id_pegawai", idPegawaiFinal));
                    paramss.add(new Pair("kesigapan", kesigapan));
                    paramss.add(new Pair("patroli", patroli));
                    paramss.add(new Pair("layanan_tamu", layanan_tamu));
                    paramss.add(new Pair("keramahan", keramahan));
                    paramss.add(new Pair("kerajinan", kerajinan));
                    paramss.add(new Pair("kebersihan", kebersihan));
                    paramss.add(new Pair("nilai_akhir", nilai_akhir));
                }
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getQuery(paramss));
                writer.flush();
                writer.close();
                os.close();

                conn.connect();
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line=br.readLine()) != null) {
                        response+=line;
                    }
                } else {
                    response = "";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            p.cancel();
            if (result != "") {
                Log.d("Ar-Rafi Log", result.replace("null", ""));
                try {
                    if (aksi.equals("get_username")) {
                        JSONArray json = new JSONArray(result.replace("null",""));
                        listUsername.clear();
                        listNama.clear();
                        listIdPegawai.clear();
                        for(int i = 0;i < json.length(); i++) {
                            listUsername.add(json.getJSONObject(i).getString("username"));
                            listNama.add(json.getJSONObject(i).getString("nama"));
                            listIdPegawai.add(json.getJSONObject(i).getString("id_pegawai"));
                        }
                        listUsernameAdapter = new ArrayAdapter<String>(InputPenilaianActivity.this, android.R.layout.simple_spinner_item, listNama);
                        listUsernamePenilaian.setAdapter(listUsernameAdapter);

                        username_ = listUsername.get(listUsernamePenilaian.getSelectedItemPosition());
                        aksi = "get_absensi";
                        new ConnectionHelper().execute();
                    } else if (aksi.equals("get_absensi")) {
                        JSONArray json = new JSONArray(result.replace("null",""));
                        for(int i = 0;i < json.length(); i++) {
                            inAbsensi.setText(json.getJSONObject(i).getString("absensi"));
                        }
                    } else {
                        JSONObject json = new JSONObject(result.replace("null", ""));
                        if (json.getString("success").equals("1")) {
                            Toast.makeText(InputPenilaianActivity.this, "Input berhasil", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(InputPenilaianActivity.this, "Error", Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(InputPenilaianActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(InputPenilaianActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getQuery(List<Pair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Pair<String, String> pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.first, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.second, "UTF-8"));
        }

        return result.toString();
    }
}
