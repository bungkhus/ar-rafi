package cukil.ar_rafi;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by syauqiilham on 6/18/16.
 */
public class AdapterPeristiwa extends BaseAdapter implements ListAdapter {
    private final Activity activity;
    private final JSONArray jsonArray;

    public AdapterPeristiwa(Activity activity, JSONArray jsonArray) {
        assert activity != null;
        assert jsonArray != null;
        this.jsonArray = jsonArray;
        this.activity = activity;
    }
    @Override
    public int getCount() {
        if (jsonArray == null)
            return 0;
        else
            return jsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        if (jsonArray == null)
            return null;
        else
            return jsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        JSONObject jsonObject = getItem(position);
        return jsonObject.optInt("id_peristiwa");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(R.layout.list_peristiwa, null);
            holder = new ViewHolder();
            holder.tTglPeristiwa = (TextView) convertView.findViewById(R.id.tTglPeristiwa);
            holder.tJamPeristiwa = (TextView) convertView.findViewById(R.id.tJamPeristiwa);
            holder.tKejadian = (TextView) convertView.findViewById(R.id.tKejadian);
            holder.tBukti = (TextView) convertView.findViewById(R.id.tBukti);
            holder.tDeskripsi = (TextView) convertView.findViewById(R.id.tDeskripsi);
            holder.bVidPeristiwa = (ImageView) convertView.findViewById(R.id.bVidPeristiwa);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final JSONObject jsonObject = getItem(position);
        if (jsonObject != null) {
            holder.tTglPeristiwa.setText(jsonObject.optString("tgl_peristiwa"));
            holder.tJamPeristiwa.setText(jsonObject.optString("jam_peristiwa"));
            holder.tKejadian.setText(jsonObject.optString("kejadian"));
            holder.tBukti.setText(jsonObject.optString("saksi"));
            holder.tDeskripsi.setText(jsonObject.optString("deskripsi"));
            holder.bVidPeristiwa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String uriString = "http://"+activity.getResources().getString(R.string.ipaddr)+"/arrafibr/video/"+jsonObject.optString("video")+".mp4";
//                    String uriString = "http://"+activity.getResources().getString(R.string.ipaddr)+"/arrafibr/video/progress_ui_rahma.mov";
                    Uri intentUri = Uri.parse(uriString);

                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(intentUri, "video/*");
                    activity.startActivity(intent);
                }
            });
        }

        return convertView;
    }

    class ViewHolder {
        TextView tTglPeristiwa;
        TextView tJamPeristiwa;
        TextView tKejadian;
        TextView tBukti;
        TextView tDeskripsi;
        ImageView bVidPeristiwa;
    }
}

