package cukil.ar_rafi;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by syauqiilham on 6/18/16.
 */
public class AdapterPenilaian extends BaseAdapter implements ListAdapter {
    private final Activity activity;
    private final JSONArray jsonArray;
    private final String pilihan;

    public AdapterPenilaian(Activity activity, JSONArray jsonArray, String pilihan) {
        assert activity != null;
        assert jsonArray != null;
        assert pilihan != null;
        this.jsonArray = jsonArray;
        this.activity = activity;
        this.pilihan = pilihan;
    }
    @Override
    public int getCount() {
        if (jsonArray == null)
            return 0;
        else
            return jsonArray.length();
    }

    @Override
    public JSONObject getItem(int position) {
        if (jsonArray == null)
            return null;
        else
            return jsonArray.optJSONObject(position);
    }

    @Override
    public long getItemId(int position) {
        JSONObject jsonObject = getItem(position);
        return jsonObject.optInt("id_presensi");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(R.layout.list_penilaian, null);
            holder = new ViewHolder();
            holder.tNamaPegawai = (TextView) convertView.findViewById(R.id.tNamaPegawai);
            holder.tAbsensi = (TextView) convertView.findViewById(R.id.tAbsensi);
            holder.tKesigapan = (TextView) convertView.findViewById(R.id.tKesigapan);
            holder.tKeramahan = (TextView) convertView.findViewById(R.id.tKeramahan);
            holder.tPatroli = (TextView) convertView.findViewById(R.id.tPatroli);
            holder.tLayananTamu = (TextView) convertView.findViewById(R.id.tLayananTamu);
            holder.tKerajinan = (TextView) convertView.findViewById(R.id.tKerajinan);
            holder.tKebersihan = (TextView) convertView.findViewById(R.id.tKebersihan);
            holder.tTotal = (TextView) convertView.findViewById(R.id.tTotal);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        JSONObject jsonObject = getItem(position);
        if (jsonObject != null) {
            if(pilihan.equalsIgnoreCase("SATPAM")){
                holder.tKebersihan.setVisibility(View.GONE);
                holder.tKerajinan.setVisibility(View.GONE);
                holder.tPatroli.setVisibility(View.VISIBLE);
                holder.tLayananTamu.setVisibility(View.VISIBLE);
            }else {
                holder.tKebersihan.setVisibility(View.VISIBLE);
                holder.tKerajinan.setVisibility(View.VISIBLE);
                holder.tPatroli.setVisibility(View.GONE);
                holder.tLayananTamu.setVisibility(View.GONE);
            }
            holder.tNamaPegawai.setText(jsonObject.optString("nama"));
            holder.tAbsensi.setText(String.format("%.2f",Double.parseDouble(jsonObject.optString("absensi"))));
            holder.tKesigapan.setText(jsonObject.optString("kesigapan"));
            holder.tKeramahan.setText(jsonObject.optString("keramahan"));
            holder.tPatroli.setText(jsonObject.optString("patroli"));
            holder.tLayananTamu.setText(jsonObject.optString("layanan_tamu"));
            holder.tKerajinan.setText(jsonObject.optString("kerajinan"));
            holder.tKebersihan.setText(jsonObject.optString("kebersihan"));
            holder.tTotal.setText(String.format("%.2f",Double.parseDouble(jsonObject.optString("nilai_akhir"))));
        }

        return convertView;
    }

    static class ViewHolder {
        TextView tNamaPegawai;
        TextView tAbsensi;
        TextView tKesigapan;
        TextView tKeramahan;
        TextView tPatroli;
        TextView tLayananTamu;
        TextView tKerajinan;
        TextView tKebersihan;
        TextView tTotal;
    }
}

