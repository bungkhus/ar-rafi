package cukil.ar_rafi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by syauqiilham on 6/17/16.
 */
public class ContentPenilaian extends Fragment {
    ListView listPenilaian;
    String response, akun;
    ArrayList<String> jadwalList = new ArrayList<String>();
    AdapterPenilaian adapterPenilaian;
    TextView tNoData;
    TextView tPatroli;
    TextView tLayananTamu;
    TextView tKerajinan;
    TextView tKebersihan;
    String tglAbsen;
    SimpleDateFormat dateFormat;
    Spinner listBulanPeriode, listTahunPeriode, spinner;
    String pilih = "SATPAM";
    String username = "", bulan, tahun;
    FloatingActionButton fab;
    boolean isloaded = false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_penilaian,container,false);
        listPenilaian = (ListView) v.findViewById(R.id.listPenilaian);
        tNoData = (TextView) v.findViewById(R.id.tNoData);
        tPatroli = (TextView) v.findViewById(R.id.tPatroli);
        tLayananTamu = (TextView) v.findViewById(R.id.tLayananTamu);
        tKerajinan = (TextView) v.findViewById(R.id.tKerajinan);
        tKebersihan = (TextView) v.findViewById(R.id.tKebersihan);
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final Calendar c = Calendar.getInstance();
        tglAbsen = dateFormat.format(c.getTime());
        bulan = new SimpleDateFormat("MM").format(c.getTime());
        tahun = new SimpleDateFormat("yyyy").format(c.getTime());
        spinner = (Spinner) v.findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                pilih = adapterView.getSelectedItem().toString();
                onPilihJadwal(pilih);

                new ConnectionHelper().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        fab = (FloatingActionButton) v.findViewById(R.id.fab);
        final Bundle b = this.getArguments();
        akun = b.getString("status");
        if (!b.getString("status").equals("katu")) {
            username = b.getString("username");
            fab.setVisibility(View.GONE);
            spinner.setVisibility(View.GONE);
            if(b.getString("status").equals("caraka")){
                spinner.setSelection(1);
            }
        }else {
            username = "";
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), InputPenilaianActivity.class);
                Bundle bi = new Bundle();
                bi.putString("username", username);
                bi.putString("status", b.getString("status"));
                i.putExtras(bi);
                startActivity(i);
            }
        });

        listBulanPeriode = (Spinner) v.findViewById(R.id.listBulanPeriode);
        listTahunPeriode = (Spinner) v.findViewById(R.id.listTahunPeriode);

        listBulanPeriode.setSelection((Integer.parseInt(bulan) - 1));
        listTahunPeriode.setSelection(1);

        listBulanPeriode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!isloaded) return;
                bulan = String.valueOf(position + 1);
                new ConnectionHelper().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        listTahunPeriode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!isloaded) return;
                tahun = listTahunPeriode.getSelectedItem().toString();
                new ConnectionHelper().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return v;
    }

    private void onPilihJadwal(String jadwal){
        if(jadwal.equalsIgnoreCase("SATPAM")){
            tKebersihan.setVisibility(View.GONE);
            tKerajinan.setVisibility(View.GONE);
            tPatroli.setVisibility(View.VISIBLE);
            tLayananTamu.setVisibility(View.VISIBLE);
        }else {
            tKebersihan.setVisibility(View.VISIBLE);
            tKerajinan.setVisibility(View.VISIBLE);
            tPatroli.setVisibility(View.GONE);
            tLayananTamu.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        new ConnectionHelper().execute();
    }

    private class ConnectionHelper extends AsyncTask<String, Void, String> {
        ProgressDialog p = new ProgressDialog(getActivity());
        @Override
        protected void onPreExecute() {
            response = "";
            p.setTitle("Tunggu sebentar...");
            p.show();
        }

        @Override
        protected String doInBackground(String... params) {
            URL url = null;
            try {
                if(akun.equalsIgnoreCase("katu")){
                    url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=getAllPenilaianByPeriode");
                }else{
                    url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=getAllPenilaianByPeriodeAndUsername");
                }
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                List<Pair> paramss = new ArrayList<Pair>();
                paramss.add(new Pair("bln", bulan));
                paramss.add(new Pair("thn", tahun));
                paramss.add(new Pair("bagian", pilih.toLowerCase()));
                paramss.add(new Pair("username", username));

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getQuery(paramss));
                writer.flush();
                writer.close();
                os.close();

                conn.connect();
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line=br.readLine()) != null) {
                        response+=line;
                    }
                } else {
                    response = "";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            p.cancel();
            if (result != "") {
                Log.d("Ar-Rafi Log", result.replace("null",""));
                try {
                    JSONArray json = new JSONArray(result.replace("null",""));
                    adapterPenilaian = new AdapterPenilaian(getActivity(), json, pilih);
                    listPenilaian.setAdapter(adapterPenilaian);
                    if (json.length() == 0)
                        tNoData.setVisibility(View.VISIBLE);
                    else
                        tNoData.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Tidak ada data", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
            }
            isloaded = true;
        }
    }

    private String getQuery(List<Pair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Pair<String, String> pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.first, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.second, "UTF-8"));
        }

        return result.toString();
    }
}
