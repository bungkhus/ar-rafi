package cukil.ar_rafi;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.renderscript.ScriptGroup;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InputPresensiActivity extends AppCompatActivity {
    ListView listPresensi;
    Map<String, Integer> listPresensis = new HashMap<String, Integer>();
    String response, aksi="get";
    RadioGroup rgAbsensi;
    EditText inAlasan;
    Spinner listUsernamePresensi, listPenugasanPresensi;
    Button bInputAbsen;
    TextView tUsernamePresensi, tPenugasanPresensi, tPresensi, tAlasan;
    ArrayAdapter<String> listUsernameAdapter, listJadwalAdapter;
    ArrayList<String> listUsername = new ArrayList<String>();
    ArrayList<String> listIdPegawai = new ArrayList<String>();
    ArrayList<String> listJadwal = new ArrayList<String>();
    ArrayList<String> listIdJadwal = new ArrayList<String>();

    String tglFinal, idPegawaiFinal, idJadwalFinal, presensiFinal, alasanFinal, tgl_mulai;
    String username, status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_presensi);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tUsernamePresensi = (TextView) findViewById(R.id.tUsernamePresensi);
        tPenugasanPresensi = (TextView) findViewById(R.id.tPenugasanPresensi);
        tPresensi = (TextView) findViewById(R.id.tPresensi);
        tAlasan = (TextView) findViewById(R.id.tAlasan);

        listUsernamePresensi = (Spinner) findViewById(R.id.listUsernamePresensi);
        listPenugasanPresensi = (Spinner) findViewById(R.id.listPenugasanPresensi);
        rgAbsensi = (RadioGroup) findViewById(R.id.rgAbsen);
        inAlasan = (EditText) findViewById(R.id.inAlasanAbsen);
        bInputAbsen = (Button) findViewById(R.id.bInputAbsen);
        bInputAbsen.setText("MASUK");
        bInputAbsen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                tglFinal = df.format(c.getTime());

                switch(rgAbsensi.indexOfChild(findViewById(rgAbsensi.getCheckedRadioButtonId()))) {
                    case 0:
                        presensiFinal = "hadir";
                        break;
                    case 1:
                        presensiFinal = "izin";
                        break;
                    case 2:
                        presensiFinal = "sakit";
                        break;
                    case 3:
                        presensiFinal = "alfa";
                        break;
                    default:
                        break;
                }

                idPegawaiFinal = listIdPegawai.get(listUsernamePresensi.getSelectedItemPosition());
                idJadwalFinal = listIdJadwal.get(listPenugasanPresensi.getSelectedItemPosition());

                alasanFinal = inAlasan.getText().toString();

                aksi = "set";
                new ConnectionHelper().execute();
            }
        });
        Bundle b = getIntent().getExtras();
        username = b.getString("username");
        status = b.getString("status");

        if (!status.equals("katu")) {
            tUsernamePresensi.setVisibility(View.GONE);
            listUsernamePresensi.setVisibility(View.GONE);
            tPresensi.setVisibility(View.GONE);
            rgAbsensi.setVisibility(View.GONE);
            tAlasan.setVisibility(View.GONE);
            inAlasan.setVisibility(View.GONE);
        }

        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
        tgl_mulai = myFormat.format(new Date());

        aksi = "get_username";
        new ConnectionHelper().execute();
    }

    private class ConnectionHelper extends AsyncTask<String, Void, String> {
        ProgressDialog p = new ProgressDialog(InputPresensiActivity.this);
        @Override
        protected void onPreExecute() {
            response = "";
            p.setTitle("Tunggu sebentar...");
            p.show();
        }

        @Override
        protected String doInBackground(String... params) {
            URL url = null;
            try {
                List<Pair> paramss = new ArrayList<Pair>();
                if (aksi.equals("get_username")) {
                    if (status.equals("katu"))
                        url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=getAllPegawai");
                    else {
                        url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=getAllPegawaiByUsername");
                        paramss.add(new Pair("username", username));
                    }
                } else if (aksi.equals("get_jadwal")) {
                    if (status.equals("katu"))
                        url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=getAllJadwal");
                    else {
                        url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=getAllJadwalByUsernameAndDate&username="+username+"&tgl_mulai="+tgl_mulai);
                    }
                }
                else {
                    url = new URL("http://"+getResources().getString(R.string.ipaddr)+"/arrafibr/api.php?act=setPresensi");
                    paramss.add(new Pair("tglAbsen", tglFinal));
                    paramss.add(new Pair("status", presensiFinal));
                    paramss.add(new Pair("alasan", alasanFinal));
                    paramss.add(new Pair("id_pegawai", idPegawaiFinal));
                    paramss.add(new Pair("id_jadwal", idJadwalFinal));

                }
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getQuery(paramss));
                writer.flush();
                writer.close();
                os.close();

                conn.connect();
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line=br.readLine()) != null) {
                        response+=line;
                    }
                } else {
                    response = "";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            p.cancel();
            if (result != "") {
                Log.d("Ar-Rafi Log", result.replace("null", ""));
                try {
                    if (aksi.equals("get_username")) {
                        JSONArray json = new JSONArray(result.replace("null",""));
                        for(int i = 0;i < json.length(); i++) {
                            listUsername.add(json.getJSONObject(i).getString("username"));
                            listIdPegawai.add(json.getJSONObject(i).getString("id_pegawai"));
                        }
                        listUsernameAdapter = new ArrayAdapter<String>(InputPresensiActivity.this, android.R.layout.simple_spinner_item, listUsername);
                        listUsernamePresensi.setAdapter(listUsernameAdapter);

                        aksi = "get_jadwal";
                        new ConnectionHelper().execute();
                    } else if (aksi.equals("get_jadwal")) {
                        JSONArray json = new JSONArray(result.replace("null",""));
                        for(int i = 0;i < json.length(); i++) {
                            listJadwal.add(json.getJSONObject(i).getString("tugas"));
                            listIdJadwal.add(json.getJSONObject(i).getString("id_jadwal"));
                        }
                        listJadwalAdapter = new ArrayAdapter<String>(InputPresensiActivity.this, android.R.layout.simple_spinner_item, listJadwal);
                        listPenugasanPresensi.setAdapter(listJadwalAdapter);
                    } else {
                        JSONObject json = new JSONObject(result.replace("null", ""));
                        if (json.getString("success").equals("1")) {
                            Toast.makeText(InputPresensiActivity.this, "Input berhasil", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(InputPresensiActivity.this, "Error", Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(InputPresensiActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(InputPresensiActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getQuery(List<Pair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Pair<String, String> pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.first, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.second, "UTF-8"));
        }

        return result.toString();
    }
}
